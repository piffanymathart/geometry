package Geometry.Lines;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point4d;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Line4dTest {

	private final Point4d P1 = new Point4d(11,12,13,14);
	private final Point4d P2 = new Point4d(21,22,23,24);

	@Test
	void twoPointConstructor() {
		ILine4d line = new Line4d(P1, P2);
		assertEquals(P1, line.getP1());
		assertEquals(P2, line.getP2());
	}

	@Test
	void copyConstructor() {
		ILine4d line1 = new Line4d(P1, P2);
		ILine4d line2 = new Line4d(line1);
		assertEquals(P1, line2.getP1());
		assertEquals(P2, line2.getP2());
	}

	@Test
	void getP1() {
		ILine4d line = new Line4d(P1, P2);
		assertEquals(P1, line.getP1());
	}

	@Test
	void getP2() {
		ILine4d line = new Line4d(P1, P2);
		assertEquals(P2, line.getP2());
	}

	@Test
	void equals_same() {
		ILine4d line1 = new Line4d(P1, P2);
		ILine4d line2 = new Line4d(P1, P2);
		assert(line1.equals(line1));
		assert(line1.equals(line2));
	}

	@Test
	void equals_different() {
		Point4d p3 = new Point4d(-999,-999,-999,-999);
		ILine4d line1 = new Line4d(P1, P2);
		ILine4d line2 = new Line4d(P1, p3);
		ILine4d line3 = new Line4d(p3, P2);
		assert(!line1.equals(null));
		assert(!line1.equals(new Object()));
		assert(!line1.equals(line2));
		assert(!line1.equals(line3));
	}
}