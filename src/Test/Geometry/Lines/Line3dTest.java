package Geometry.Lines;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Line3dTest {

	private final Point3d P1 = new Point3d(11,12,13);
	private final Point3d P2 = new Point3d(21,22,23);

	@Test
	void twoPointConstructor() {
		ILine3d line = new Line3d(P1, P2);
		assertEquals(P1, line.getP1());
		assertEquals(P2, line.getP2());
	}

	@Test
	void copyConstructor() {
		ILine3d line1 = new Line3d(P1, P2);
		ILine3d line2 = new Line3d(line1);
		assertEquals(P1, line2.getP1());
		assertEquals(P2, line2.getP2());
	}

	@Test
	void getP1() {
		ILine3d line = new Line3d(P1, P2);
		assertEquals(P1, line.getP1());
	}

	@Test
	void getP2() {
		ILine3d line = new Line3d(P1, P2);
		assertEquals(P2, line.getP2());
	}

	@Test
	void equals_same() {
		ILine3d line1 = new Line3d(P1, P2);
		ILine3d line2 = new Line3d(P1, P2);
		assert(line1.equals(line1));
		assert(line1.equals(line2));
	}

	@Test
	void equals_different() {
		Point3d p3 = new Point3d(-999,-999,-999);
		ILine3d line1 = new Line3d(P1, P2);
		ILine3d line2 = new Line3d(P1, p3);
		ILine3d line3 = new Line3d(p3, P2);
		assert(!line1.equals(null));
		assert(!line1.equals(new Object()));
		assert(!line1.equals(line2));
		assert(!line1.equals(line3));
	}
}