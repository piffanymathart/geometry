package Geometry.Lines;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point2d;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Line2dTest {

	private final Point2d P1 = new Point2d(11,12);
	private final Point2d P2 = new Point2d(21,22);

	@Test
	void twoPointConstructor() {
		ILine2d line = new Line2d(P1, P2);
		assertEquals(P1, line.getP1());
		assertEquals(P2, line.getP2());
	}

	@Test
	void copyConstructor() {
		ILine2d line1 = new Line2d(P1, P2);
		ILine2d line2 = new Line2d(line1);
		assertEquals(P1, line2.getP1());
		assertEquals(P2, line2.getP2());
	}

	@Test
	void getP1() {
		ILine2d line = new Line2d(P1, P2);
		assertEquals(P1, line.getP1());
	}

	@Test
	void getP2() {
		ILine2d line = new Line2d(P1, P2);
		assertEquals(P2, line.getP2());
	}

	@Test
	void equals_same() {
		ILine2d line1 = new Line2d(P1, P2);
		ILine2d line2 = new Line2d(P1, P2);
		assert(line1.equals(line1));
		assert(line1.equals(line2));
	}

	@Test
	void equals_different() {
		Point2d p3 = new Point2d(-999,-999);
		ILine2d line1 = new Line2d(P1, P2);
		ILine2d line2 = new Line2d(P1, p3);
		ILine2d line3 = new Line2d(p3, P2);
		assert(!line1.equals(null));
		assert(!line1.equals(new Object()));
		assert(!line1.equals(line2));
		assert(!line1.equals(line3));
	}
}