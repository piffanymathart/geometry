package Geometry.Polygons;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point4d;

import static org.junit.jupiter.api.Assertions.*;

class Polygon4dTest {

	private final Point4d P1 = new Point4d(11, 12, 13, 14);
	private final Point4d P2 = new Point4d(21, 22, 23, 24);
	private final Point4d P3 = new Point4d(31, 32, 33, 34);

	@Test
	void pointsConstructor() {
		Point4d[] verts = null;
		IPolygon4d polygon = new Polygon4d(verts);
		assertNull(polygon.getVertices());
		assertEquals(0, polygon.size());
	}

	@Test
	void copyConstructor() {
		Point4d[] verts = {P1, P2, P3};
		IPolygon4d polygon1 = new Polygon4d(verts);
		IPolygon4d polygon2 = new Polygon4d(polygon1);
		assertArrayEquals(verts, polygon2.getVertices());
	}

	@Test
	void getVertices() {
		Point4d[] verts = {P1, P2, P3};
		IPolygon4d polygon = new Polygon4d(verts);
		assertArrayEquals(verts, polygon.getVertices());
	}

	@Test
	void size() {
		Point4d[] verts = {P1, P2, P3};
		IPolygon4d polygon = new Polygon4d(verts);
		assertEquals(verts.length, polygon.size());
	}

	@Test
	void getVertex() {
		IPolygon4d polygon = new Polygon4d(P1, P2, P3);
		assertThrows(RuntimeException.class, () -> polygon.get(-1));
		assertEquals(P1, polygon.get(0));
		assertEquals(P2, polygon.get(1));
		assertEquals(P3, polygon.get(2));
		assertThrows(RuntimeException.class, () -> polygon.get(3));
	}

	@Test
	void equals_same() {
		Point4d[] verts = {P1, P2, P3};
		IPolygon4d polygon1 = new Polygon4d(verts);
		IPolygon4d polygon2 = new Polygon4d(verts);
		assert(polygon1.equals(polygon1));
		assert(polygon1.equals(polygon2));
	}

	@Test
	void equals_different() {
		IPolygon4d polygon1 = new Polygon4d(P1, P2, P3);
		IPolygon4d polygon2 = new Polygon4d(P1, P2);
		IPolygon4d polygon3 = new Polygon4d(P1, P2, P3, P2);
		IPolygon4d polygon4 = new Polygon4d(P3, P2, P3);
		IPolygon4d polygon5 = new Polygon4d(P1, P3, P3);
		IPolygon4d polygon6 = new Polygon4d(P1, P2, P1);
		assert(!polygon1.equals(null));
		assert(!polygon1.equals(new Object()));
		assert(!polygon1.equals(polygon2));
		assert(!polygon1.equals(polygon3));
		assert(!polygon1.equals(polygon4));
		assert(!polygon1.equals(polygon5));
		assert(!polygon1.equals(polygon6));
	}
}