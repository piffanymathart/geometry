package Geometry.Polygons;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;

import static org.junit.jupiter.api.Assertions.*;

class Polygon3dTest {

	private final Point3d P1 = new Point3d(11, 12, 13);
	private final Point3d P2 = new Point3d(21, 22, 23);
	private final Point3d P3 = new Point3d(31, 32, 33);

	@Test
	void pointsConstructor() {
		Point3d[] verts = null;
		IPolygon3d polygon = new Polygon3d(verts);
		assertArrayEquals(new Point3d[0], polygon.getVertices());
		assertEquals(0, polygon.size());
	}

	@Test
	void copyConstructor() {
		Point3d[] verts = {P1, P2, P3};
		IPolygon3d polygon1 = new Polygon3d(verts);
		IPolygon3d polygon2 = new Polygon3d(polygon1);
		assertArrayEquals(verts, polygon2.getVertices());
	}

	@Test
	void getVertices() {
		Point3d[] verts = {P1, P2, P3};
		IPolygon3d polygon = new Polygon3d(verts);
		assertArrayEquals(verts, polygon.getVertices());
	}

	@Test
	void size() {
		Point3d[] verts = {P1, P2, P3};
		IPolygon3d polygon = new Polygon3d(verts);
		assertEquals(verts.length, polygon.size());
	}

	@Test
	void getVertex() {
		Point3d[] verts = {P1, P2, P3};
		IPolygon3d polygon = new Polygon3d(verts);
		assertThrows(RuntimeException.class, () -> polygon.get(-1));
		assertEquals(P1, polygon.get(0));
		assertEquals(P2, polygon.get(1));
		assertEquals(P3, polygon.get(2));
		assertThrows(RuntimeException.class, () -> polygon.get(3));
	}

	@Test
	void equals_same() {
		Point3d[] verts = {P1, P2, P3};
		IPolygon3d polygon1 = new Polygon3d(verts);
		IPolygon3d polygon2 = new Polygon3d(verts);
		assert(polygon1.equals(polygon1));
		assert(polygon1.equals(polygon2));
	}

	@Test
	void equals_different() {
		Point3d[] verts1 = {P1, P2, P3};
		Point3d[] verts2 = {P1, P2};
		Point3d[] verts3 = {P1, P2, P3, P2};
		Point3d[] verts4 = {P3, P2, P3};
		Point3d[] verts5 = {P1, P3, P3};
		Point3d[] verts6 = {P1, P2, P1};
		IPolygon3d polygon1 = new Polygon3d(verts1);
		IPolygon3d polygon2 = new Polygon3d(verts2);
		IPolygon3d polygon3 = new Polygon3d(verts3);
		IPolygon3d polygon4 = new Polygon3d(verts4);
		IPolygon3d polygon5 = new Polygon3d(verts5);
		IPolygon3d polygon6 = new Polygon3d(verts6);
		assert(!polygon1.equals(null));
		assert(!polygon1.equals(new Object()));
		assert(!polygon1.equals(polygon2));
		assert(!polygon1.equals(polygon3));
		assert(!polygon1.equals(polygon4));
		assert(!polygon1.equals(polygon5));
		assert(!polygon1.equals(polygon6));
	}
}