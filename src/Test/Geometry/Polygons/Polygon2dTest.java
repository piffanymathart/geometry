package Geometry.Polygons;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point2d;

import static org.junit.jupiter.api.Assertions.*;

class Polygon2dTest {

	private final Point2d P1 = new Point2d(11, 12);
	private final Point2d P2 = new Point2d(21, 22);
	private final Point2d P3 = new Point2d(31, 32);

	@Test
	void pointsConstructor() {
		Point2d[] verts = null;
		IPolygon2d polygon = new Polygon2d(verts);
		assertNull(polygon.getVertices());
		assertEquals(0, polygon.size());
	}

	@Test
	void copyConstructor() {
		Point2d[] verts = {P1, P2, P3};
		IPolygon2d polygon1 = new Polygon2d(verts);
		IPolygon2d polygon2 = new Polygon2d(polygon1);
		assertArrayEquals(verts, polygon2.getVertices());
	}

	@Test
	void getVertices() {
		Point2d[] verts = {P1, P2, P3};
		IPolygon2d polygon = new Polygon2d(verts);
		assertArrayEquals(verts, polygon.getVertices());
	}

	@Test
	void size() {
		Point2d[] verts = {P1, P2, P3};
		IPolygon2d polygon = new Polygon2d(verts);
		assertEquals(verts.length, polygon.size());
	}

	@Test
	void getVertex() {
		IPolygon2d polygon = new Polygon2d(P1, P2, P3);
		assertThrows(RuntimeException.class, () -> polygon.get(-1));
		assertEquals(P1, polygon.get(0));
		assertEquals(P2, polygon.get(1));
		assertEquals(P3, polygon.get(2));
		assertThrows(RuntimeException.class, () -> polygon.get(3));
	}

	@Test
	void equals_same() {
		Point2d[] verts = {P1, P2, P3};
		IPolygon2d polygon1 = new Polygon2d(verts);
		IPolygon2d polygon2 = new Polygon2d(verts);
		assert(polygon1.equals(polygon1));
		assert(polygon1.equals(polygon2));
	}

	@Test
	void equals_different() {
		IPolygon2d polygon1 = new Polygon2d(P1, P2, P3);
		IPolygon2d polygon2 = new Polygon2d(P1, P2);
		IPolygon2d polygon3 = new Polygon2d(P1, P2, P3, P2);
		IPolygon2d polygon4 = new Polygon2d(P3, P2, P3);
		IPolygon2d polygon5 = new Polygon2d(P1, P3, P3);
		IPolygon2d polygon6 = new Polygon2d(P1, P2, P1);
		assert(!polygon1.equals(null));
		assert(!polygon1.equals(new Object()));
		assert(!polygon1.equals(polygon2));
		assert(!polygon1.equals(polygon3));
		assert(!polygon1.equals(polygon4));
		assert(!polygon1.equals(polygon5));
		assert(!polygon1.equals(polygon6));
	}
}