package Geometry.Lines;

import javax.vecmath.Point4d;

/**
 * A 4D line.
 */
public interface ILine4d {

	/**
	 * Gets a copy of the first point.
	 * @return The first point.
	 */
	Point4d getP1();

	/**
	 * Gets a copy of the second point.
	 * @return The second point.
	 */
	Point4d getP2();

	/**
	 * Checks if the object is equal to the line.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the line.
	 */
	@Override
	boolean equals(Object obj);
}
