package Geometry.Lines;

import javax.vecmath.Point3d;

/**
 * A 3D line.
 */
public interface ILine3d {

	/**
	 * Gets a copy of the first point.
	 * @return The first point.
	 */
	Point3d getP1();

	/**
	 * Gets a copy of the second point.
	 * @return The second point.
	 */
	Point3d getP2();

	/**
	 * Checks if the object is equal to the line.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the line.
	 */
	@Override
	boolean equals(Object obj);
}
