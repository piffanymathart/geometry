package Geometry.Lines;

import javax.vecmath.Point2d;

/**
 * A 2D line.
 */
public interface ILine2d {

	/**
	 * Gets a copy of the first point.
	 * @return The first point.
	 */
	Point2d getP1();

	/**
	 * Gets a copy of the second point.
	 * @return The second point.
	 */
	Point2d getP2();

	/**
	 * Checks if the object is equal to the line.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the line.
	 */
	@Override
	boolean equals(Object obj);
}
