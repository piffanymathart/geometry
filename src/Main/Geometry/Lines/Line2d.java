package Geometry.Lines;

import javax.vecmath.Point2d;

/**
 * A 2D line.
 */
public class Line2d implements ILine2d {

	/**
	 * The first point.
	 */
	protected Point2d m_p1;

	/**
	 * The second point.
	 */
	protected Point2d m_p2;

	/**
	 * Constructs a Line2d from two specified points.
	 * @param p1 The first point.
	 * @param p2 The second point.
	 */
	public Line2d(Point2d p1, Point2d p2) {
		m_p1 = p1;
		m_p2 = p2;
	}

	/**
	 * Copy constructor.
	 * @param line The line to copy from.
	 */
	public Line2d(ILine2d line) {
		m_p1 = line.getP1();
		m_p2 = line.getP2();
	}

	/**
	 * Gets a copy of the first point.
	 * @return The first point.
	 */
	public Point2d getP1() {
		return m_p1==null ? null : new Point2d(m_p1);
	}

	/**
	 * Gets a copy of the second point.
	 * @return The second point.
	 */
	public Point2d getP2() {
		return m_p2==null ? null : new Point2d(m_p2);
	}

	/**
	 * Checks if the object is equal to the line.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the line.
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) return false;
		if (!ILine2d.class.isAssignableFrom(obj.getClass())) return false;
		final ILine2d line = (ILine2d) obj;

		// compare two lines
		return m_p1.equals(line.getP1()) && m_p2.equals(line.getP2());
	}

}
