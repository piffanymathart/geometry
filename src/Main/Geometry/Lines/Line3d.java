package Geometry.Lines;

import javax.vecmath.Point3d;

/**
 * A 3D line.
 */
public class Line3d implements ILine3d {

	/**
	 * The first point.
	 */
	protected Point3d m_p1;

	/**
	 * The second point.
	 */
	protected Point3d m_p2;

	/**
	 * Constructs a Line3d from two specified points.
	 * @param p1 The first point.
	 * @param p2 The second point.
	 */
	public Line3d(Point3d p1, Point3d p2) {
		m_p1 = p1;
		m_p2 = p2;
	}

	/**
	 * Copy constructor.
	 * @param line The line to copy from.
	 */
	public Line3d(ILine3d line) {
		m_p1 = line.getP1();
		m_p2 = line.getP2();
	}

	/**
	 * Gets a copy of the first point.
	 * @return The first point.
	 */
	public Point3d getP1() {
		return m_p1==null ? null : new Point3d(m_p1);
	}

	/**
	 * Gets a copy of the second point.
	 * @return The second point.
	 */
	public Point3d getP2() {
		return m_p2==null ? null : new Point3d(m_p2);
	}

	/**
	 * Checks if the object is equal to the line.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the line.
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) return false;
		if (!ILine3d.class.isAssignableFrom(obj.getClass())) return false;
		final ILine3d line = (ILine3d) obj;

		// compare two lines
		return m_p1.equals(line.getP1()) && m_p2.equals(line.getP2());
	}

}
