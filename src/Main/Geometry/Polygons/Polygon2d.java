package Geometry.Polygons;

import javax.vecmath.Point2d;

/**
 * A 2d polygon.
 */
public class Polygon2d implements IPolygon2d {

	/**
	 * The vertices.
	 */
	protected Point2d[] m_vertices;

	/**
	 * Constructs a polygon from a set of vertices.
	 * @param vertices The vertices.
	 */
	public Polygon2d(Point2d... vertices) {
		setVertices(vertices);
	}

	/**
	 * Copy constructor.
	 * @param polygon The polygon to copy from.
	 */
	public Polygon2d(IPolygon2d polygon) {
		setVertices(polygon.getVertices());
	}

	/**
	 * Gets a copy of the vertices.
	 * @return The vertices.
	 */
	public Point2d[] getVertices(){

		if(m_vertices==null) return null;

		int n = m_vertices.length;
		Point2d[] vertices = new Point2d[n];
		for(int i=0; i<n; i++) {
			vertices[i] = new Point2d(m_vertices[i]);
		}
		return vertices;
	}

	/**
	 * Gets the number of vertices in the polygon.
	 * @return The number of vertices.
	 */
	public int size() {
		return m_vertices==null ? 0 : m_vertices.length;
	}

	/**
	 * Gets a copy of the specified vertex.
	 * @param index The index of the vertex.
	 * @return The specified vertex.
	 */
	public Point2d get(int index) {
		return new Point2d(m_vertices[index]);
	}

	/**
	 * Checks if the object is equal to the polygon.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the polygon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IPolygon2d.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IPolygon2d) obj);
	}

	/**
	 * Checks if the two polygons are equal.
	 * @param polygon The polygon with which to compare.
	 * @return Whether the polygons are equal.
	 */
	private boolean equals(IPolygon2d polygon) {

		if(size() != polygon.size()) return false;
		for(int i=0; i<size(); i++) {
			if(!m_vertices[i].equals(polygon.get(i))) return false;
		}
		return true;
	}

	/**
	 * Sets the values of the vertices.
	 * @param vertices The vertices to set values from.
	 */
	private void setVertices(Point2d[] vertices) {
		if(vertices == null) {
			m_vertices = null;
			return;
		}
		int n = vertices.length;
		m_vertices = new Point2d[n];
		for(int i=0; i<n; i++) {
			m_vertices[i] = vertices[i];
		}
	}
}
