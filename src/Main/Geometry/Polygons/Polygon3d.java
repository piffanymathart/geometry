package Geometry.Polygons;

import javax.vecmath.Point3d;

/**
 * A 3d polygon.
 */
public class Polygon3d implements IPolygon3d {

	/**
	 * The vertices.
	 */
	protected Point3d[] m_vertices;

	/**
	 * Constructs a polygon from a set of vertices.
	 * @param vertices The vertices.
	 */
	public Polygon3d(Point3d... vertices) {
		setVertices(vertices);
	}

	/**
	 * Copy constructor.
	 * @param polygon The polygon to copy from.
	 */
	public Polygon3d(IPolygon3d polygon) {
		setVertices(polygon.getVertices());
	}

	/**
	 * Gets a copy of the vertices.
	 * @return The vertices.
	 */
	public Point3d[] getVertices(){

		if(m_vertices==null) return new Point3d[0];

		int n = m_vertices.length;
		Point3d[] vertices = new Point3d[n];
		for(int i=0; i<n; i++) {
			vertices[i] = new Point3d(m_vertices[i]);
		}
		return vertices;
	}

	/**
	 * Gets the number of vertices in the polygon.
	 * @return The number of vertices.
	 */
	public int size() {
		return m_vertices==null ? 0 : m_vertices.length;
	}

	/**
	 * Gets a copy of the specified vertex.
	 * @param index The index of the vertex.
	 * @return The specified vertex.
	 */
	public Point3d get(int index) {
		return new Point3d(m_vertices[index]);
	}

	/**
	 * Checks if the object is equal to the polygon.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the polygon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IPolygon3d.class.isAssignableFrom(obj.getClass())) return false;
		return equals((IPolygon3d) obj);
	}

	/**
	 * Checks if the two polygons are equal.
	 * @param polygon The polygon with which to compare.
	 * @return Whether the polygons are equal.
	 */
	private boolean equals(IPolygon3d polygon) {

		if(size() != polygon.size()) return false;
		for(int i=0; i<size(); i++) {
			if(!m_vertices[i].equals(polygon.get(i))) return false;
		}
		return true;
	}

	/**
	 * Sets the values of the vertices.
	 * @param vertices The vertices to set values from.
	 */
	private void setVertices(Point3d[] vertices) {
		if(vertices == null) {
			m_vertices = null;
			return;
		}
		int n = vertices.length;
		m_vertices = new Point3d[n];
		for(int i=0; i<n; i++) {
			m_vertices[i] = vertices[i];
		}
	}
}
