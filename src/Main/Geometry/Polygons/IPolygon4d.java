package Geometry.Polygons;

import javax.vecmath.Point4d;

/**
 * A 4D polygon.
 */
public interface IPolygon4d {

	/**
	 * Gets a copy of the vertices.
	 * @return The vertices.
	 */
	Point4d[] getVertices();

	/**
	 * Gets the number of vertices in the polygon.
	 * @return The number of vertices.
	 */
	int size();

	/**
	 * Gets a copy of the specified vertex.
	 * @param index The index of the vertex.
	 * @return The specified vertex.
	 */
	Point4d get(int index);

	/**
	 * Checks if the object is equal to the polygon.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the polygon.
	 */
	@Override
	boolean equals(Object obj);
}
